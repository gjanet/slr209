import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
/**
 * Handles multiple inputs, is used when multiple files are being read at the same time.
 * @author guillaume
 *
 */
public class InputHandler {
	
	private ArrayList<InputInterface> inputInterfaces;

	/**
	 * Class constructor. for now, only mode 0 : FileInput is implemented. new modes need to be added to createInputList
	 * @see createInputList
	 * @param inputModes gives the list of input modes chosen.
	 * @param inputPaths 
	 * @param bufferSizes 
	 */
	public InputHandler(ArrayList<Integer> inputModes, ArrayList<String> inputPaths, ArrayList<Integer> bufferSizes) {			
		inputInterfaces = createInputList(inputModes, inputPaths, bufferSizes);
	}
	
	/**
	 * class Constructor.  for now, only mode 0 : FileInput is implemented. new modes need to be added to createInputList
	 * uses a settings file instead of the straight arrays of the other constructor
	 * @param settingsFile path to the file containing the information for the program.
	 */
	public InputHandler(String settingsFile) {
		ArrayList<Integer> inputModes = new ArrayList<Integer>();
		ArrayList<String> inputPaths = new ArrayList<String>();
		ArrayList<Integer> bufferSizes = new ArrayList<Integer>();
		try {
			FileReader fr= new FileReader(settingsFile);
			BufferedReader br = new BufferedReader(fr);
			String line;
			String[] split;
			while((line = br.readLine()) != null) {
				split = line.split("=");
				switch(split[0]) {
					case "InputMode" :
						inputModes.add(Integer.parseInt(split[1]));
						break;
					case "InputPath" :
						inputPaths.add(split[1]);
						break;
					case "BufferSize" :
						bufferSizes.add(Integer.parseInt(split[1]));
					default :
						break;
				}
			}
			br.close();
		} catch (IOException e) {e.printStackTrace();}
		inputInterfaces = createInputList(inputModes, inputPaths, bufferSizes);
	}
	/**
	 * creates the list of inputInterfaces
	 * @param inputModes
	 * @param inputPaths
	 * @param bufferSizes
	 * @return
	 */
	private ArrayList<InputInterface> createInputList(ArrayList<Integer> inputModes, ArrayList<String> inputPaths, ArrayList<Integer> bufferSizes){
		ArrayList<InputInterface> out = new ArrayList<InputInterface>();
		assert(inputModes.size() == inputPaths.size() && inputPaths.size() == bufferSizes.size());
		for(int i = 0; i != inputModes.size(); i++) {
			switch(inputModes.get(i)) {
				case 0 :
					out.add(new FileInput(inputPaths.get(i), bufferSizes.get(0)));
					break;
				default :
					System.err.println("invalid input mode. Skipping...");
			}
		}
		return out;
	}
	
	public InputInterface getInputInterface(int i) {return inputInterfaces.get(i);}
	public int size() {return inputInterfaces.size();}
}
