import java.util.ArrayList;

public interface OutputInterface {

	/**
	 * prints the result to the given output
	 * @param period period to show
	 * @param offset 
	 * @param source automata from which the data comes
	 */
	public void printResult(ArrayList<Integer> period, ArrayList<Integer> offset, String source);
	/**
	 * returns the output of the class
	 * @return 
	 */
	public String showOutput();
	
	public void clearOutput();
	/**
	 * prints the string event to the output along with the source
	 * @param event
	 * @param source
	 */
	public void printEvent(String event, String source);
	
}
