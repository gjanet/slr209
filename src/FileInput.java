import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class FileInput implements InputInterface {
	
	private int bufferSize;
	private String filePath;
	ArrayList<Integer> stateId = new ArrayList<Integer>();
	private int lineNumber=0;
	boolean fileEnd = false;
	
	public FileInput(String filePath, int bufferSize) {
		this.bufferSize = bufferSize;
		this.filePath = filePath;
	}
	
	@Override
	public int getBufferSize() {
		return bufferSize;
	}

	@Override
	public ArrayList<Integer> getBuffer() {
		FileReader fr;
		String line;
		String[] splitLine;
		int i = 0;
		try {
			fr= new FileReader(filePath);
			BufferedReader br = new BufferedReader(fr);
			for(int j = 0; j<lineNumber; i++)//get to the line where the last buffer left off
				br.readLine();
			br.readLine();
			while((line = br.readLine()) != null && i < bufferSize) {
				splitLine = line.split(",");
				stateId.add(Integer.parseInt(splitLine[splitLine.length -1].replace("-", "")));
				i++;
				lineNumber++;
			}
			if(line == null)
					fileEnd = true;
			br.close();
		} catch (IOException e) {e.printStackTrace();}
		return stateId;
	}
	
	@Override
	public boolean sourceEnded() {
		return fileEnd;
	}
	
	@Override
	public String getInputSource() {
		return filePath;
	}


}
