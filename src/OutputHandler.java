import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class OutputHandler {
	private ArrayList<OutputInterface> outputInterfaces;
	/**
	 * Class constructor. for now, only mode 0 : FileOutput is implemented. new modes need to be added to createOutputList
	 * @see createOutputList
	 * @param OutputModes gives the list of input modes chosen.
	 * @param outputPaths  
	 */
	public OutputHandler(ArrayList<Integer> outputModes, ArrayList<String> outputPaths) {			
		outputInterfaces = createOutputList(outputModes, outputPaths);
	}
	
	/**
	 * sets outputs based on the given settings file
	 * @param settingsFile path to the file containing settings information.
	 */
	public OutputHandler(String settingsFile) {
		ArrayList<Integer> outputModes = new ArrayList<Integer>();
		ArrayList<String> outputPaths = new ArrayList<String>();
		try {
			FileReader fr= new FileReader(settingsFile);
			BufferedReader br = new BufferedReader(fr);
			String line;
			String[] split;
			while((line = br.readLine()) != null) {
				split = line.split("=");
				switch(split[0]) {
					case "OutputMode" :
						outputModes.add(Integer.parseInt(split[1]));
						break;
					case "OutputPath" :
						outputPaths.add(split[1]);
						break;
					default :
						break;
				}
			}
			br.close();
		} catch (IOException e) {e.printStackTrace();}
		outputInterfaces = createOutputList(outputModes, outputPaths);
	}
	/**
	 * creates the output list based on the given mode and path arrays.
	 * @param outputModes
	 * @param outputPaths
	 * @return
	 */
	private ArrayList<OutputInterface> createOutputList(ArrayList<Integer> outputModes, ArrayList<String> outputPaths){
		ArrayList<OutputInterface> out = new ArrayList<OutputInterface>();
		assert(outputModes.size() == outputPaths.size());
		for(int i = 0; i != outputModes.size(); i++) {
			switch(outputModes.get(i)) {
				case 0 :
					out.add(new FileOutput(outputPaths.get(i)));
					break;
				default :
					System.err.println("outvalid output mode. Skipping...");
			}
		}
		return out;
	}
	
	public void clearOutputs() {
		for(OutputInterface out : outputInterfaces)
			out.clearOutput();
	}
	public OutputInterface getOutputInterface(int i) {return outputInterfaces.get(i);}
	public int size() {return outputInterfaces.size();}
}
