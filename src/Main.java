import java.util.ArrayList;
import java.util.Random;
public class Main {
	
	public static void main(String[] args) throws InterruptedException {
		CycleDetectorHandler detector = new CycleDetectorHandler("/home/guillaume/slr209/slr209/src/settings.cfg");
		detector.start();
		detector.join();
		}
	
	/**
	 * creates a randomly generated array that eventually becomes cyclic if CycleNbr is over 0
	 * @return returns the generated ArrayList
	 */
	private static ArrayList<Integer> createPeriodicArray(){
		
		ArrayList<Integer> array = new ArrayList<Integer>();
		Random rd = new Random();
		ArrayList<Integer> period = new ArrayList<Integer>();
		int offsetBound = rd.nextInt(200);
		int cycleLength = rd.nextInt(100);
		int CycleNbr = rd.nextInt(20);
		for(int i = 0; i<offsetBound; i++)
			array.add(rd.nextInt(60));
		for(int i = 0; i<cycleLength; i++)
			period.add(rd.nextInt(60));
		for(int i = 0; i<CycleNbr; i++)
			array.addAll(period);
		return array;
	}
}
