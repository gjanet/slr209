import java.util.ArrayList;

public interface InputInterface {
	
		/**
		 * 
		 * @return  max size of the returned buffer
		 */
		public int getBufferSize();
		/**
		 * 
		 * @return ArrayList<Integer> from input source
		 */
		public ArrayList<Integer> getBuffer();
		public String getInputSource();
		/**
		 * 
		 * @return true if the source has been fully read
		 */
		public boolean sourceEnded();

}
