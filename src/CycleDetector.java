import java.util.ArrayList;
import java.util.HashMap;


/**
 * class meant to be used as static. contains the cycle detection method
 * @author guillaume
 *
 */
public class CycleDetector {

	/**
	 * Finds cycles in an array of integers.
	 * Could be easily adapted for use with other data types as there are only "equal" comparisons.
	 * @param list the list of integers in which to find a cycle
	 * @return returns an ArrayList containing the start index of the cycle and its period
	 */
	public static ArrayList<Integer> detection(ArrayList<Integer> list){
		long start = System.nanoTime();
		ArrayList<Integer> out = new ArrayList<Integer>();
		int n = list.size();
		int offset = -1;;
		int period = 0;
		boolean stop = false;

		//checking which period is right
		for(period = n/3;period != 1; period--) {
			//check wether the same number appears after the tested period
			for(int i = 1; i<2; i++) {
				if(list.subList(n-period, n).equals(list.subList(n-(i+1)*period, n-i*period))) {
					stop = true;
					break;
				}
			}
			if(stop) break;
		}
		//find out whether period is the actual period or a multiple of it
		ArrayList<Integer> periodList = new ArrayList<Integer>(list.subList(n-period, n));
		HashMap<Integer, Integer> histogram = new HashMap<Integer, Integer>();
		int min = Integer.MAX_VALUE;
		for(int num : periodList) {
			if(histogram.containsKey(num))
				histogram.put(num, histogram.get(num)+1);
			else
				histogram.put(num, 1);
		}
		for(int num : histogram.values())
			if(num<min) min = num;
		if(min != 1) {
			if(periodList.subList(0, periodList.size()/min).equals(periodList.subList(periodList.size()/min,
			                                                                        2*(periodList.size()/min))))
				period/=min;
		}

		if(!stop) {
			out.add(-1);
			out.add(-1);
		}else {
			periodList = new ArrayList<Integer>(periodList.subList(0, period));
			//find the offset
			for(int i = 0; i<n-period; i++){
				if(list.subList(i, i+period).equals(periodList)) {
					offset = i-period;
					break;
				}
			}
			offset++;
			out.add(offset);
			out.add(period);
		}
		//System.out.println(out);
		System.err.println(System.nanoTime()-start);
		return out;
	}

}

