import java.util.ArrayList;

public class CycleDetectorHandler {
	private InputHandler inputs;
	private OutputHandler outputs;
	private ArrayList<CycleDetectorRunnable> detectors;
	private ArrayList<Thread> detectorThreads;
	
	public CycleDetectorHandler(String settingsFile) {
		inputs = new InputHandler(settingsFile);
		outputs = new OutputHandler(settingsFile);
		detectors = new ArrayList<CycleDetectorRunnable>();
		outputs.clearOutputs();
		detectorThreads = new ArrayList<Thread>();
		for(int i = 0; i<inputs.size(); i++) {
			if(outputs.size() == 1)
				detectors.add(new CycleDetectorRunnable(inputs.getInputInterface(i), outputs.getOutputInterface(0)));
			else {
				assert(inputs.size() == outputs.size());
				detectors.add(new CycleDetectorRunnable(inputs.getInputInterface(i), outputs.getOutputInterface(i)));
			}
		}
		for(CycleDetectorRunnable detector : detectors)
			detectorThreads.add(new Thread(detector));
	}
	
	public void start() {
		for(Thread detectorThread : detectorThreads)
			detectorThread.start();
	}
	
	public void join() throws InterruptedException {
		for(Thread detectorThread : detectorThreads)
			detectorThread.join();
	}
	
	
	
}
