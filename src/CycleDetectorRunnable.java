import java.util.ArrayList;
/**
 * handles the IO of the cycle detector and makes it Runnable in order to be used as a Thread
 * @author guillaume
 *
 */
public class CycleDetectorRunnable implements Runnable {

	private InputInterface input = null;
	private OutputInterface output = null;


	public CycleDetectorRunnable(InputInterface in, OutputInterface out) {
		input = in;
		output = out;
	}
	@Override
	public void run() {
		ArrayList<Integer> period = new ArrayList<Integer>();
		ArrayList<Integer> array = new ArrayList<Integer>();
		ArrayList<Integer> out = new ArrayList<Integer>();
		ArrayList<Integer> offset = new ArrayList<Integer>();
		ArrayList<Integer> currentOut = new ArrayList<Integer>();
		ArrayList<Integer> currentPeriod = new ArrayList<Integer>();
		//loop until the end of the source is reached
		do{

			array = input.getBuffer();

			currentOut = CycleDetector.detection(array);
			if(currentOut.contains(-1)) {
				output.printEvent("no cycle", input.getInputSource());
			}else {
				offset.addAll(array.subList(0, currentOut.get(0)));
				currentPeriod.addAll(array.subList(currentOut.get(0), currentOut.get(0)+currentOut.get(1)));
				if(out.size() == 0 || out.contains(-1)) {//if this is the first detection, output it.
					period.addAll(array.subList(currentOut.get(0), currentOut.get(0)+currentOut.get(1)));
					out = currentOut;
					offset = new ArrayList<Integer>(array.subList(0, out.get(0)));
					output.printResult(currentPeriod, offset, input.getInputSource());
				}else if(currentPeriod.equals(period)) {
					offset = (ArrayList<Integer>) array.subList(0, out.get(0)-1);
					//if the new output is the same as the old one, check whether there was a short stop in the cycle
					if(offset.size()>period.size() || !offset.equals(period.subList(period.size()-offset.size(), period.size()-1))) {
						output.printEvent("irregularity in period but cycle continued", input.getInputSource());
					}else {
						offset = (ArrayList<Integer>) array.subList(0, out.get(0)-1);
						out = currentOut;
						period.clear();
						period.addAll(array.subList(currentOut.get(0), currentOut.get(0)+currentOut.get(1)));
					}		
				}else {
					output.printEvent("cycle changed", input.getInputSource());
					output.printResult(currentPeriod, offset, input.getInputSource());
				}
			}
			
		} while(!input.sourceEnded());
		System.out.println("done with input " + input.getInputSource());
	}	
}
