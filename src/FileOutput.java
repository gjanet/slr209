import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class FileOutput implements OutputInterface{

	private String filePath;
	private FileWriter fw = null;
	private BufferedWriter bw;
	private PrintWriter pw;

	public FileOutput(String filePath) {
		this.filePath = filePath;
		try {
			fw = new FileWriter(filePath, true);
		} catch (IOException e) {e.printStackTrace();}

	}

	@Override
	public synchronized void printResult(ArrayList<Integer> period, ArrayList<Integer> offset, String source) {
		try {
			fw = new FileWriter(filePath, true);
		} catch (IOException e) {e.printStackTrace();}
		bw = new BufferedWriter(fw);
		pw = new PrintWriter(bw);
		pw.print("period from file : " + source + " is ");
		pw.print(period);
		if(offset.size() != 0) {
			pw.print(" which starts at index : ");
			pw.println(offset);
		}else
			pw.println(" with no offset");
		pw.print("\n");
		pw.close();
	}

	@Override
	public String showOutput() {
		return filePath;
	}

	@Override
	public synchronized void clearOutput() {
		File f = new File(filePath);
		f.delete();

	}

	@Override
	public synchronized void printEvent(String event, String source) {
		try {
			fw = new FileWriter(filePath, true);
		} catch (IOException e) {e.printStackTrace();}
		bw = new BufferedWriter(fw);
		pw = new PrintWriter(bw);
		pw.print("event from file " + source + " : ");
		pw.println(event);
		pw.close();
	}
}
